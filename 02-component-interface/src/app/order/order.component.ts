import { Component, Input } from '@angular/core';
import { Order } from "../model/order";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html'
})
export class OrderComponent {

  @Input() order: Order;
}
