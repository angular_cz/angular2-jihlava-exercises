import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { SaleComponent } from './sale/sale.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    SaleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],

  // TODO 1.1 - registrujte službu na úrovni modulu
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
