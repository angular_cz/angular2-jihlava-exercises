import { Component } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabset',
  templateUrl: './tabset.component.html'
})
export class TabsetComponent {

  private tabs: TabComponent[] = [];

  // TODO 3.2 - získejte tab, který má být vybrán

  // TODO 3.3 - zobrazte vybraný tab

  // TODO 4.1 - reagujte na stisk klávesy
  onKey(code: string) {

    if (!code.startsWith('Digit')) {
      return
    }

    let digit = parseInt(code.replace('Digit', ''));
    if (digit === 0) {
      digit = 10;
    }

    this.selectTab(digit - 1);
  }

  add(tab: TabComponent) {
    this.tabs.push(tab);
  }

  selectTab(selectedTab: TabComponent);
  selectTab(selectedTab: number);

  selectTab(selectedTab: TabComponent | number) {

    if (typeof selectedTab === 'number') {
      selectedTab = this.tabs[selectedTab];
    }

    if (selectedTab) {
      this.tabs.forEach(tab => tab.hide());
      selectedTab.show();
    }
  }

  remove(tabComponent: TabComponent) {
    const position = this.tabs.indexOf(tabComponent);
    if (position > -1) {
      this.tabs.splice(position, 1);

      let newSelectedPosition = Math.min(position, this.tabs.length - 1);
      this.selectTab(newSelectedPosition);
    }
  }

}
