import { Component, OnInit } from '@angular/core';
import { ChatRoom, Message } from "../model/chat";
import { Response } from "@angular/http";

import { Observable, Subject } from "rxjs";
import 'rxjs/add/operator/startWith'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'
import { ActivatedRoute, Params } from "@angular/router";
import { ChatService } from "../chat.service";

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html'
})
export class ChatDetailComponent implements OnInit {

  chatRoomId: number = 1;
  chatRoom: ChatRoom;
  myMessages$ = new Subject<Message>();
  myMessagesStream$: Observable<Response>;

  routeParams$: Observable<Params>;

  constructor(private chatService: ChatService,
              private route: ActivatedRoute) {

    // TODO 3.1 - načtěte id ze snapshotu parametrů
    // TODO 3.2 - reagujte na změnu parametrů
    // TODO 3.3 - vytvořte stream změny parametrů

    this.myMessagesStream$ = this.myMessages$
      .flatMap(message => this.chatService.sendMessage(this.chatRoomId, message))
  }

  ngOnInit(): void {

    // TODO 3.4 - připojte stream změny parametrů

    Observable.merge(Observable.interval(5000),
      this.myMessagesStream$
    )
      .startWith(null)
      .flatMap(() => this.chatService.getRoomById(this.chatRoomId))
      .do(() => console.log('chatReloaded'))
      .subscribe((chatRoom) => this.chatRoom = chatRoom);
  }

  sendMyMessage(message: Message) {
    this.myMessages$.next(message);
  }
}
