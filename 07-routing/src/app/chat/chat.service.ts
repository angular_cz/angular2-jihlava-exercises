import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";

import 'rxjs/add/operator/map';
import { Observable } from "rxjs";
import { Room, Message, ChatRoom } from "./model/chat";

@Injectable()
export class ChatService {

  private readonly ROOMS_URL = 'http://localhost:8000/rooms';

  private rooms$;

  constructor(private http: Http) {
    this.rooms$ = this.http.get(this.ROOMS_URL)
      .map(response => response.json()).share();
  }

  getRooms(): Observable<Room[]> {
    return this.rooms$;
  }

  getRoomById(roomId: number): Observable<ChatRoom> {
    return this.http.get(this.ROOMS_URL + '/' + roomId)
      .map(response => response.json())
  }

  sendMessage(roomId:number, message: Message):Observable<Response> {
    return this.http.post(this.ROOMS_URL + '/' + roomId, message)
  }

}
