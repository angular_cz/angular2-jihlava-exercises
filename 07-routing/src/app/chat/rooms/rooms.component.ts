import { Component } from '@angular/core';
import { ChatService } from "../chat.service";
import { Room } from "../model/chat";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html'
})
export class RoomsComponent{

  rooms: Room[];

  // TODO 2.7 - načtěte data z routy
  constructor(chatService: ChatService) {
    chatService.getRooms()
      .subscribe(rooms => this.rooms = rooms,
                  error => console.log("chatService.getRooms error", error))
  }

}
