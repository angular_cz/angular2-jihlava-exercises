import { Injectable, Optional } from '@angular/core';
import { Book } from './model/book';

@Injectable()
export class CalculatorService {

  constructor(@Optional() private basePrice: number) {
    if (this.basePrice === null) {
      this.basePrice = 10;
    }
  }

  savedCalculations: Book[] = [];

  calculate(book: Book) {
    const colorPagePrice = 10;
    const normalPagePrice = 1;
    const letterOnCoverPrice = 2;

    let price = this.basePrice;

    const normalPages = book.totalPages - book.colorPages;

    price += book.title.length * letterOnCoverPrice;
    price += normalPages * normalPagePrice;
    price += book.colorPages * colorPagePrice;

    return price;
  }

  saveCalculation(book: Book) {
    book.price = this.calculate(book);
    this.savedCalculations.push(book);
  }
}

export const saleCalculatorProvider = {
  provide: CalculatorService, useFactory: () => new CalculatorService(0)
};
