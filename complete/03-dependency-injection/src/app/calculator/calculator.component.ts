import { Component } from '@angular/core';
import { Book } from '../model/book';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {

  book: Book;

  constructor(private calculator : CalculatorService){
    this.clearCalculator()
  }

  private clearCalculator() {
    this.book = {
      title: 'Book title',
      totalPages: 0,
      colorPages: 0
    }
  }

  getPrice() {
    return this.calculator.calculate(this.book);
  }

  saveCalculation() {
    this.calculator.saveCalculation(this.book);
    this.clearCalculator();
  }

  getCalculations() {
    return this.calculator.savedCalculations;
  }
}
