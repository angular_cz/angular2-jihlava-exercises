import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { saleCalculatorProvider } from '../calculator.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css'],
  providers: [saleCalculatorProvider]
})
export class SaleComponent  {}
