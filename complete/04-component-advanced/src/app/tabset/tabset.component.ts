import { Component, ContentChild, HostListener, AfterContentInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabset',
  templateUrl: './tabset.component.html',
  styleUrls: ['./tabset.component.css']
})
export class TabsetComponent implements AfterContentInit{

  private tabs: TabComponent[] = [];

  @ContentChild('selectedTab') selectedTab: TabComponent;

  constructor() {
  }

  ngAfterContentInit() {
    this.selectTab(this.selectedTab);
  }

  add(tab: TabComponent) {
    this.tabs.push(tab);
  }

  selectTab(selectedTab: TabComponent);
  selectTab(selectedTab: number);

  selectTab(selectedTab: TabComponent | number) {

    if (typeof selectedTab === 'number') {
      selectedTab = this.tabs[selectedTab];
    }

    if (selectedTab) {
      this.tabs.forEach(tab => tab.hide());
      selectedTab.show();
    }
  }

  remove(tabComponent: TabComponent) {
    const position = this.tabs.indexOf(tabComponent);
    if (position > -1) {
      this.tabs.splice(position, 1);

      this.selectTab(this.tabs.length - 1);
    }
  }

  @HostListener('document:keyup', ['$event.code'])
  onKey(code: string) {

    if (!code.startsWith('Digit')) {
      return
    }

    let digit = parseInt(code.replace('Digit', ''));
    if (digit === 0) {
      digit = 10;
    }

    this.selectTab(digit - 1);
  }

}
