import { User } from './user';
export const cloneUser = (user: User) : User => {
  const newUser = Object.assign({}, user);
  newUser.address = Object.assign({}, user.address);

  return newUser;
}
