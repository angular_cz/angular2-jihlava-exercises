export interface Address {
  street: string;
  city: string;
  postalCode: string;
}

export interface User {
  name: string;
  surname: string;
  twitterName: string;
  sex: "male" | "female";

  address: Address;

  registrationDate: Date;
}
