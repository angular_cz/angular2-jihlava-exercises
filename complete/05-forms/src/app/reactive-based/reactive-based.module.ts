import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { UserReactiveFormComponent } from './user-reactive-form/user-reactive-form.component';
import { SharedModule } from "../shared/shared.module";
import { FormControlErrorsComponent } from './form-control-errors/form-control-errors.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [
    UserReactiveFormComponent,
    FormControlErrorsComponent
  ],
  exports: [
    UserReactiveFormComponent
  ]
})
export class ReactiveBasedModule { }
