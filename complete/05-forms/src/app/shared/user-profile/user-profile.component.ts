import { Component, Input } from '@angular/core';
import { User } from '../../model/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent {

  @Input() user:User ;

  getSexLetter() {
    return this.user.sex[0].toUpperCase();
  }
}
