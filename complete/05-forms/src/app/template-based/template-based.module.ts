import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileFormComponent } from './user-profile-form/user-template-based-form.component';
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { FormModelErrorsComponent } from './form-model-errors/form-model-errors.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    UserProfileFormComponent,
    FormModelErrorsComponent
  ],
  exports: [
    UserProfileFormComponent
   ]
})
export class TemplateBasedModule {
}
