import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { User } from '../../model/user';
import { cloneUser } from '../../model/clone';

@Component({
  selector: 'app-user-template-based-form',
  templateUrl: 'user-template-based-form.component.html',
  styleUrls: ['user-template-based-form.component.css']
})
export class UserProfileFormComponent {

  @Input('user') set userFromInput (userFromInput: User) {
    this.user = cloneUser(userFromInput);
  };

  @Output() userSubmit = new EventEmitter<User>();

  user: User;

  formSubmit(form) {
    console.log('onSubmit', form);
    if (form.valid) {
      this.userSubmit.emit(this.user);
    }
  }

}
