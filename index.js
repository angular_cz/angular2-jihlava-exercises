const restify = require('restify');

const rooms = require('./rooms');

const server = restify.createServer();
server.use(restify.CORS());
server.use(restify.bodyParser());

server.get('/', restify.serveStatic({
  directory: __dirname,
  default: 'index.html'
}));

server.get('/rooms', (req, res, next) => {
  res.send(rooms.getRoomList());
  next();
});

server.get('/rooms/:id', (req, res, next) => {

  let id = parseInt(req.params.id);

  if (rooms.hasRoom(id)) {
    res.send(rooms.getRoom(id));
  } else {
    res.send(new restify.ForbiddenError('Room with id ' + id + ' does not exists'));
  }

  next();
});

server.post('/rooms/:id', (req, res, next) => {
  let id = parseInt(req.params.id);

  rooms.newMessage(id, req.body);
  console.log('user add new message to#', id, req.body);

  res.send(201);
  next();
});


server.listen(8000, function() {
  console.log('%s listening at %s', server.name, server.url);
});
